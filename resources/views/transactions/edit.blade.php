@extends('layouts.base')

@section('content')

    <form action="{{ route('transactions.update', ['transaction' => $transaction]) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="flex py-2">
            <label class="w-1/6 text-gray-600" for="description">Description</label>
            <input class="flex flex-grow px-2 py-1 rounded border" id="description" name="description" type="text" value="{{ $transaction->description }}">
        </div>

        <div class="flex py-2">
            <label class="w-1/6 text-gray-600" for="amount">Amount</label>
            <input class="flex-grow px-2 py-1 rounded border" id="amount" name="amount" type="number" value="{{ $transaction->amount }}">
        </div>

        <div class="flex py-2">
            <label class="w-1/6 text-gray-600" for="date">Date</label>
            <input class="flex-grow px-2 py-1 rounded border" id="date" name="date" type="date" value="{{ $transaction->date }}">
        </div>

        <div class="flex py-2">
            <label class="w-1/6 text-gray-600">Type</label>
            <div class="flex-grow">
                <label for="income">
                    <input class="px-2 py-1 rounded border" id="income" name="in_out" value="1" type="radio" {{ !$transaction->in_out ? 'checked' : ''  }}> Income
                </label>
                <label for="outcome">
                    <input class="px-2 py-1 rounded border" id="outcome" name="in_out" value="0" type="radio" {{ $transaction->in_out ? 'checked' : ''  }}> Outcome
                </label>
            </div>
        </div>

        <div class="flex py-4 mt-4 border-t border-400">
            <button type="submit" class="text-white px-4 py-2 bg-blue-600 rounded">Update</button>
        </div>
    </form>

@endsection
