@extends('layouts.base')

@section('content')

    <header class="w-full block py-6">
        <a class="px-6 py-2 rounded bg-blue-700 text-white" href="{{ route('transactions.create') }}">Create</a>
    </header>

    <table class="table-auto w-full pt-10">
        <thead>
            <tr>
                <th class="py-2 px-2 w-px">#</th>
                <th class="py-2 px-2 w-px">Date</th>
                <th class="py-2 px-2">Description</th>
                <th class="py-2 px-2 w-1/12">Amount</th>
                <th class="py-2 px-2 w-1/12">Action</th>
            </tr>
        </thead>

        <tbody>
            @foreach($data as $i => $row)
            @php
                $groups = $data->where('date_only', $row->date_only);
                $first = $groups->first();
                $count = $groups->count();
            @endphp
            <tr>
                <td class="border px-2 py-2 text-center">{{ $i + 1 }}</td>
                @if($first->id == $row->id)
                    <td rowspan="{{ $count }}" class="border px-2 py-2 text-center">{{ $row->date_only }}</td>
                @endif
                <td class="border px-2 py-2">{{ $row->description }}</td>
                <td class="border px-2 py-2 text-right">{{ $row->amount_string }}</td>
                <td class="border px-2 py-2 text-center">
                    <a class="btn-edit" href="{{ route('transactions.edit', ['transaction' => $row->id]) }}">Edit</a>
                    <a class="btn-delete" data-desc="{{ $row->description }}" href="{{ route('transactions.destroy', ['transaction' => $row->id]) }}">Delete</a>
                </td>
            </tr>
            @endforeach
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td class="border px-2 py-2 text-right" colspan="3">Total Earning</td>
                <td class="border px-2 py-2 text-right">{{ $sumary->earning }}</td>
                <td class="border px-2 py-2">&nbsp;</td>
            </tr>
            <tr>
                <td class="border px-2 py-2 text-right" colspan="3">Total Expense</td>
                <td class="border px-2 py-2 text-right">{{ $sumary->expense }}</td>
                <td class="border px-2 py-2">&nbsp;</td>
            </tr>
            <tr>
                <td class="border px-2 py-2 text-right" colspan="3">Current Balance</td>
                <td class="border px-2 py-2 text-right">{{ $sumary->balance }}</td>
                <td class="border px-2 py-2">&nbsp;</td>
            </tr>
        </tbody>
    </table>

@endsection
