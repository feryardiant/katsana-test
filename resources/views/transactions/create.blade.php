@extends('layouts.base')

@section('content')

    <form action="{{ route('transactions.store') }}" method="POST">
        @csrf

        <div class="flex py-2">
            <label class="w-1/6 text-gray-600" for="description">Description</label>
            <input class="flex flex-grow px-2 py-1 rounded border" id="description" name="description" type="text" value="{{ old('description') }}">
        </div>

        <div class="flex py-2">
            <label class="w-1/6 text-gray-600" for="amount">Amount</label>
            <input class="flex-grow px-2 py-1 rounded border" id="amount" name="amount" type="number" value="{{ old('amount') }}">
        </div>

        <div class="flex py-2">
            <label class="w-1/6 text-gray-600" for="date">Date</label>
            <input class="flex-grow px-2 py-1 rounded border" id="date" name="date" type="date" value="{{ old('date', date('Y-m-d')) }}">
        </div>

        <div class="flex py-2">
            <label class="w-1/6 text-gray-600">Type</label>
            <div class="flex-grow">
                <label for="income">
                    <input class="px-2 py-1 rounded border" id="income" name="in_out" value="1" type="radio" {{ old('in_out') === 1 ? 'checked' : '' }}> Income
                </label>
                <label for="outcome">
                    <input class="px-2 py-1 rounded border" id="outcome" name="in_out" value="0" type="radio" {{ old('in_out') === 0 ? 'checked' : '' }}> Outcome
                </label>
            </div>
        </div>

        <div class="flex py-4 mt-4 border-t border-400">
            <button type="submit" class="text-white px-4 py-2 bg-blue-600 rounded">Submit</button>
        </div>
    </form>

@endsection
