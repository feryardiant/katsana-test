@extends('layouts.base')

@section('content')

    <form action="{{ route('login') }}" method="POST">
        @csrf

        <div class="flex py-2">
            <label class="w-1/6 text-gray-600" for="email">Login</label>
            <input class="flex flex-grow px-2 py-1 rounded border" id="email" name="email" type="email">
        </div>

        <div class="flex py-2">
            <label class="w-1/6 text-gray-600" for="password">Password</label>
            <input class="flex-grow px-2 py-1 rounded border" id="password" name="password" type="password">
        </div>

        <div class="flex py-4 mt-4 border-t border-400">
            <button type="submit" class="text-white px-4 py-2 bg-blue-600 rounded">login</button>
        </div>
    </form>

@endsection
