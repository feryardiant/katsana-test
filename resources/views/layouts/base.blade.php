<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="antialiased text-gray-900 leading-snug">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Dompet</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">

        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body class="min-h-screen">
        <div id="app" class="min-h-screen">
            <header class="border-b border-gray-300 z-100 h-12 w-full mb-4">
                <div class="container mx-auto flex">
                    <a class="w-1/6 text-xl leading-normal font-bold block py-2" href="#">Acme</a>
                    <div class="flex flex-grow py-3">
                        <a href="{{ route('transactions.index') }}">Transactions</a>
                    </div>
                </div>
            </header>

            <main class="w-full">
                <div class="container mx-auto">
                    @yield('content')
                </div>
            </main>
        </div>

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
