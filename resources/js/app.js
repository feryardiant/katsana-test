require('./bootstrap');

const $delete = document.querySelectorAll('.btn-delete');

$delete.forEach($btn => {
    $btn.addEventListener('click', e => {
        e.preventDefault()

        if (!confirm(`Are you sure want to delete ${e.target.getAttribute('data-desc')}?`)) {
            return
        }

        axios.delete(e.target.href).then(res => {
            alert(res.data.message)
            window.location = res.data.redirect
        }).catch(e => console.error(e))
    })
})
