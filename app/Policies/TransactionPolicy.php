<?php

namespace App\Policies;

use App\Transaction;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Transaction $transaction)
    {
        return $user->id === $transaction->owner_id;
    }

    public function create()
    {
        return true;
    }

    public function update(User $user, Transaction $transaction)
    {
        return $this->view($user, $transaction);
    }

    public function delete(User $user, Transaction $transaction)
    {
        return $this->view($user, $transaction);
    }
}
