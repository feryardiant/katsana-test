<?php

namespace App\Http\Requests\Transactions;

use App\Transaction;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('transaction'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'amount' => 'required|numeric',
            'in_out' => 'required|boolean',
            'description' => 'string|nullable',
        ];
    }

    public function update(Transaction $transaction)
    {
        $transaction->update($this->validated());

        return $transaction;
    }
}
