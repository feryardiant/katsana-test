<?php

namespace App\Http\Requests\Transactions;

use App\Transaction;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create', new Transaction);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'amount' => 'required|numeric',
            'in_out' => 'required|boolean',
            'description' => 'string|nullable',
        ];
    }

    public function save()
    {
        $data = $this->validated();

        $data['owner_id'] = $this->user()->id;

        return Transaction::create($data);
    }
}
