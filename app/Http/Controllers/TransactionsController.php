<?php

namespace App\Http\Controllers;

use App\Http\Requests\Transactions\CreateRequest;
use App\Http\Requests\Transactions\UpdateRequest;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request Request instance
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        /* @var Transaction[] $data Transactions collection */
        $data = Transaction::where('owner_id', $request->user()->id)->get();

        $sumary = (object) [
            'earning' => 0,
            'expense' => 0,
            'balance' => 0,
        ];

        foreach ($data as $row) {
            if ($row->in_out) {
                $sumary->earning += $row->amount;
            } else {
                $sumary->expense += $row->amount;
            }
        }

        $sumary->balance = $sumary->earning - $sumary->expense;

        return view('transactions.index', compact('data', 'sumary'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('transactions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $transaction Transaction create request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $transaction)
    {
        $message = $transaction->save()
            ? 'New data created'
            : 'Failed to create new data';

        return redirect()->route('transactions.index')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param Transaction $transaction Transaction instance
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Transaction $transaction)
    {
        return $this->edit($transaction);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Transaction $transaction Transaction instance
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Transaction $transaction)
    {
        return view('transactions.edit', compact('transaction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request     Transaction update request
     * @param Transaction   $transaction Transaction instance
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, Transaction $transaction)
    {
        $message = $request->update($transaction)
            ? 'Data saved'
            : 'Failed to save data';

        return redirect()->route('transactions.index')->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Transaction $transaction Transaction instance.
     *
     * @return array Json response
     */
    public function destroy(Transaction $transaction)
    {
        $this->authorize('delete', $transaction);

        $redirect = route('transactions.index');
        $message = $transaction->delete()
            ? 'Data deleted'
            : 'Failed to delete the data';

        return compact('message', 'redirect');
    }
}
