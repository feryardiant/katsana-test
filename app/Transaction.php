<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Transactin model
 *
 * @property      string $date
 * @property      int $amount
 * @property      string description
 * @property      bool $in_out
 * @property      int $owner_id
 * @property      User|User[] $owner
 * @property-read string $type
 * @property-read string $amount_string
 */
class Transaction extends Model
{
    /**
     * Fillable
     */
    protected $fillable = ['date','amount','description','in_out','owner_id'];

    /**
     * Type attribute.
     *
     * @return string
     */
    public function getTypeAttribute()
    {
        return $this->in_out ? 'Earning' : 'Expense';
    }

    /**
     * The amount_string attribute
     *
     * @return string
     */
    public function getAmountStringAttribute()
    {
        $string = number_format($this->amount);

        if (!$this->in_out) {
            $string = '- '.$string;
        }

        return $string;
    }

    public function getDateOnlyAttribute()
    {
        return substr($this->date, -2);
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }
}

